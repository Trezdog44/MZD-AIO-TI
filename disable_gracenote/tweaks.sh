#!/bin/sh
# Install/uninstall kill gracenote tweak. by Trezdog44

# disable watchdog and allow write access
echo 1 > /sys/class/gpio/Watchdog\ Disable/value
mount -o rw,remount /

show_message()
{
	killall jci-dialog
	#	log_message "= POPUP: $* "
	/jci/tools/jci-dialog --info --title="MESSAGE" --text="$*" --no-cancel &
	sleep 4
}
compatibility_check()
{
	# versions > v54 is v55 and up
	_VER=$(grep "^JCI_SW_VER=" /jci/version.ini | sed 's/^.*_\([^_]*\)\"$/\1/' | cut -d '.' -f 1)
	if [ "$_VER" -gt "54" ]
	then
		echo 1
	else
		echo 0
	fi
}
MYDIR=$(dirname "$(readlink -f "$0")")
COMPAT_CHECK=$(compatibility_check)

if [ $COMPAT_CHECK -eq 0 ]; then
	show_message "=== *** INCOMPATABLE VERSION *** ==="
	exit 1
fi
msg="Disable Gracenote Tweak\nGracenote is currently "
if [ -z "$(pgrep gracenote_server)" ];then
	msg="${msg}Disabled\n"
else
	msg="${msg}Active\n"
fi
msg="${msg}Choose Installation Method"
/jci/tools/jci-dialog --3-button-dialog --title="Disable Gracenote!" --text="${msg}" --ok-label="DISABLE GN" --cancel-label="ENABLE GN" --button3-label="SKIP"
choice=$?

if [ $choice -eq 0 ]; then

	if [ ! -e /jci/scripts/stage_usb.sh.org ]; then
		cp -a /jci/scripts/stage_usb.sh /jci/scripts/stage_usb.sh.org
	fi

	cp -a ${MYDIR}/disable_gracenote/* /jci/scripts/
	chmod 777 /jci/scripts/stage_usb.sh
	chmod 777 /jci/scripts/kill_gn.sh
	show_message "=====***** DISABLED GRACENOTE *****====="
	sleep 5
elif [ $choice -eq 1 ]; then
	if [ -e /jci/scripts/stage_usb.sh.org ]; then
		cp -a /jci/scripts/stage_usb.sh.org /jci/scripts/stage_usb.sh
	fi
	rm -f /jci/scripts/kill_gn.sh
	show_message "=====**** ENABLED GRACENOTE *****===="
else
	show_message "=======****** SKIPPED ******======="
fi
if [ $choice -ne 2 ]; then
	show_message "===**** REBOOTING ****==="
	sleep 2
	reboot
fi

exit 0
