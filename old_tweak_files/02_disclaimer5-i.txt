backup_org /jci/gui/apps/system/js/systemApp.js
# reduce disclaimer time
show_message "REDUCE DISCLAIMER TIME ..."
log_message "========******  INSTALL REDUCE DISCLAIMER TO 0.5 SECONDS ... *******====="

# Modify systemApp.js
sed -i 's/this._disclaimerTime.remaining = 3500/this._disclaimerTime.remaining = 500/g' /jci/gui/apps/system/js/systemApp.js
log_message "===                  Disclaimer Reduced to 0.5 Seconds                ==="

log_message "=======******* END INSTALLATION OF DISCLAIMER TO .5 SECONDS *******======"
log_message " "
