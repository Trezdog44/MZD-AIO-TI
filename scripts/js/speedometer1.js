/* jshint -W033, -W116, -W117 */
var lastGPSspeedValue = 0
var lastEnginespeedValue = 0
var lastGearPositionValue = 0
var lastOutCarTempValue = 0
var lastIntakeTempValue = 0
var lastCoolantTempValue = 0
var lastAvgFuelValue = 0
var speedCurrent = 0
var speedSumTotal = 0
var speedTop = 0
var speedAvg = 0
var speedValue = 0
var totalTripSeconds = 0
var totalIdleSeconds = 0
var totalMoveSeconds = 0
var carspd = 0
var backgroundColor = ''

function initSpeedometer () {
  setInterval(function () {
    updateSpeedAll()
    updateSpeedGPS()
    updateAvgFuel()
    updateEngineSpeed()
    updateGearPosition()
    updateOutCarTemp()
    updateIntakeTemp()
    updateCoolantTemp()
  }, 1000)
}

function updateSpeedAvg (speed) {
  totalMoveSeconds++
  speedSumTotal += speed
  var avgSpeed = Math.ceil(speedSumTotal / totalMoveSeconds)
  if (speedAvg !== avgSpeed) {
    $('#speedAvgValue').html(avgSpeed)
  }
}

function updateSpeedTop (speed) {
  if (speed > speedTop) {
    speedTop = speed
    $('#speedTopValue').html(speedTop)
  }
}

function updateSpeedAll () {
  $.get('/tmp/root/CarSpeed', function (data) {
    data = $.trim(data)
    data = Number(data)
    if ($.isNumeric(data)) {
      data = data * 0.01
      if (data < 1.0) {
        data = 0
      }
    }
    if ($.isNumeric(data) && data !== speedValue) {
      if (data !== 0) {
        speedValue = data + 4
      } else {
        speedValue = data
      }
      carspd = speedValue
      var speedTemp = Math.ceil(speedValue)
      if (speedTemp > 0) {
        updateSpeedTop(speedTemp)
        updateSpeedAvg(speedTemp)
      }
      $('#speedCurrent').each(function () {
        var $this = $(this)
        $({ Counter: $this.text() }).animate({ Counter: speedValue }, {
          duration: 950,
          easing: 'linear',
          step: function (now) {
            $this.text(Math.ceil(now))
            speedCurrent = $this.text()
            updateSpeedBar(speedCurrent)
          },
          complete: function () {}
        })
      })
    }
  })
}

function updateSpeedGPS () {
  $.get('/tmp/root/GpsSpeed', function (speedGPS) {
    speedGPS = $.trim(speedGPS)
    if ($.isNumeric(speedGPS)) {
      if (speedGPS < 1.0) {
        speedGPS = 0
      }
      if (speedValue < 1.0) {
        speedGPS = 0
      }
    }
    if ($.isNumeric(speedGPS) && speedGPS !== lastGPSspeedValue) {
      lastGPSspeedValue = speedGPS
      $('#gpsSpeedValue, .gpsSpeedValue').each(function () {
        var $this = $(this)
        $({ Counter: $this.text() }).animate({ Counter: lastGPSspeedValue }, {
          duration: 950,
          easing: 'linear',
          step: function (now) {
            $this.text(Math.ceil(now))
            gpsSpeedValue = $this.text()
          },
          complete: function () {}
        })
      })
    }
  })
}

function updateAvgFuel () {
  $.get('/tmp/root/AvgFuel', function (avgFuel) {
    avgFuel = $.trim(avgFuel)
    if ($.isNumeric(avgFuel) && avgFuel !== lastAvgFuelValue) {
      lastAvgFuelValue = avgFuel
      $('#avgFuelValue').each(function () {
        var $this = $(this)
        $({ Counter: $this.text() }).animate({ Counter: lastAvgFuelValue }, {
          duration: 950,
          easing: 'linear',
          step: function (now) {
            $this.text(Math.ceil(now))
            avgFuelValue = $this.text(avgFuel / 100)
          },
          complete: function () {}
        })
      })
    }
  })
}

function updateEngineSpeed () {
  $.get('/tmp/root/EngineSpeed', function (speedEngine) {
    speedEngine = $.trim(speedEngine)
    if ($.isNumeric(speedEngine) && speedEngine !== lastEnginespeedValue) {
      lastEnginespeedValue = Math.round(speedEngine * 2)
      $('#engineSpeedValue, .vehicleSpeed').each(function () {
        var $this = $(this)
        $({ Counter: $this.text() }).animate({ Counter: lastEnginespeedValue }, {
          duration: 950,
          easing: 'linear',
          step: function (now) {
            $this.text(Math.ceil(now))
            engineSpeedValue = $this.text()
          },
          complete: function () {}
        })
      })
    }
  })
}

function updateGearPosition () {
  $.get('/tmp/root/GearPosition', function (gearPos) {
    gearPos = $.trim(gearPos)
    if ($.isNumeric(gearPos) && gearPos !== lastGearPositionValue) {
      lastGearPositionValue = gearPos
      $('#gearPositionValue').each(function () {
        var $this = $(this)
        $({ Counter: $this.text() }).animate({ Counter: lastGearPositionValue }, {
          duration: 950,
          easing: 'linear',
          step: function (now) {
            $this.text(Math.ceil(now))
            gearPositionValue = $this.text()
          },
          complete: function () {}
        })
      })
    }
  })
}

function updateOutCarTemp () {
  $.get('/tmp/root/OutCarTemp', function (outTemp) {
    outTemp = $.trim(outTemp)
    if ($.isNumeric(outTemp) && outTemp !== lastOutCarTempValue) {
      // outTemp = (outTemp / 2.4);
      outTemp = Math.ceil((outTemp / 2.4) - 3)
      lastOutCarTempValue = outTemp
      $('#outsideTempValue').each(function () {
        var $this = $(this)
        $({ Counter: $this.text() }).animate({ Counter: lastOutCarTempValue }, {
          duration: 950,
          easing: 'linear',
          step: function (now) {
            $this.text(Math.ceil(now))
            outsideTempValue = $this.text()
          },
          complete: function () {}
        })
      })
    }
  })
}

function updateIntakeTemp () {
  $.get('/tmp/root/IntakeTemp', function (intTemp) {
    intTemp = $.trim(intTemp)
    if ($.isNumeric(intTemp) && intTemp !== lastIntakeTempValue) {
      intTemp = (intTemp - 28)
      intTemp = Math.ceil(intTemp)
      lastIntakeTempValue = intTemp
      $('#intakeTempValue').each(function () {
        var $this = $(this)
        $({ Counter: $this.text() }).animate({ Counter: lastIntakeTempValue }, {
          duration: 950,
          easing: 'linear',
          step: function (now) {
            $this.text(Math.ceil(now))
            intakeTempValue = $this.text()
          },
          complete: function () {}
        })
      })
    }
  })
}

function updateCoolantTemp () {
  $.get('/tmp/root/CoolantTemp', function (coolTemp) {
    coolTemp = $.trim(coolTemp)
    if ($.isNumeric(coolTemp) && coolTemp !== lastCoolantTempValue) {
      // coolTemp = (coolTemp * 32) / 1.8;
      coolTemp = (coolTemp - 40)
      coolTemp = Math.ceil(coolTemp)
      lastCoolantTempValue = coolTemp
      $('#coolantTempValue').each(function () {
        var $this = $(this)
        $({ Counter: $this.text() }).animate({ Counter: lastCoolantTempValue }, {
          duration: 950,
          easing: 'linear',
          step: function (now) {
            $this.text(Math.ceil(now))
            coolantTempValue = $this.text()
          },
          complete: function () {}
        })
      })
    }
  })
}

function updateSpeedBar (speed) {
  for (var i = 150; i >= 105; i -= 5) {
    var barClassName = '.speedBar_' + i
    if (speed >= i) {
      switch (i) {
        case 150:	backgroundColor = '#FF0000';	break
        case 145:	backgroundColor = '#FF0000';	break
        case 140:	backgroundColor = '#FF0000';	break
        case 135:	backgroundColor = '#FF0000';	break
        case 130:	backgroundColor = '#FF0000';	break
        case 125:	backgroundColor = '#FE2E2E';	break
        case 120:	backgroundColor = '#FF451C';	break
        case 115:	backgroundColor = '#FF6932';	break
        case 110:	backgroundColor = '#FE9A2E';	break
        case 105:	backgroundColor = '#FECC20';	break
      }
      $(barClassName).css({'background-color': backgroundColor})
    } else {
      $(barClassName).css({'background-color': 'transparent'})
    }
  }
  for (var i = 100; i >= 55; i -= 5) {
    var barClassName = '.speedBar_' + i
    if (speed >= i) {
      switch (i) {
        case 100:	backgroundColor = '#FFED2E';	break
        case 95:	backgroundColor = '#FFF430';	break
        case 90:	backgroundColor = '#F7FE2E';	break
        case 85:	backgroundColor = '#C8FE2E';	break
        case 80:	backgroundColor = '#9AFE2E';	break
        case 75:	backgroundColor = '#64FE2E';	break
        case 70:	backgroundColor = '#2EFE2E'; 	break
        case 65:	backgroundColor = '#2EFE64'; 	break
        case 60:	backgroundColor = '#2EFE9A';	break
        case 55:	backgroundColor = '#58FAD0';	break
      }
      $(barClassName).css({'background-color': backgroundColor})
    } else {
      $(barClassName).css({'background-color': 'transparent'})
    }
  }
  for (var i = 50; i >= 5; i -= 5) {
    var barClassName = '.speedBar_' + i
    if (speed >= i) {
      switch (i) {
        case 50:	backgroundColor = '#58FAD0';	break
        case 45:	backgroundColor = '#58FAD0';	break
        case 40:	backgroundColor = '#58FAD0';	break
        case 35:	backgroundColor = '#58FAD0';	break
        case 30:	backgroundColor = '#58FAD0';	break
        case 25:	backgroundColor = '#81F7D8';	break
        case 20:	backgroundColor = '#A9F5E1';	break
        case 15:	backgroundColor = '#CEF6EC';	break
        case 10:	backgroundColor = '#E0F8F1';	break
        case 5:		backgroundColor = '#EFFBF8';	break
      }
      $(barClassName).css({'background-color': backgroundColor})
    } else {
      $(barClassName).css({'background-color': 'transparent'})
    }
  }
}
