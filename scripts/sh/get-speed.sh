#!/bin/sh
dbus-send --print-reply --address=unix:path=/tmp/dbus_service_socket --type=method_call --dest=com.jci.lds.data /com/jci/lds/data com.jci.lds.data.GetPosition | awk 'NR==8{print $2}' > /tmp/root/GpsSpeed
smdb-read -n vdm_vdt_current_data -e EngineSpeed > /tmp/root/EngineSpeed
smdb-read -n vdm_vdt_current_data -e VehicleSpeed > /tmp/root/CarSpeed
smdb-read -n vdm_vdt_current_data -e TransmissionGearPosition > /tmp/root/GearPosition