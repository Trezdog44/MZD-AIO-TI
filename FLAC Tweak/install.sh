#!/bin/sh
echo 1 > /sys/class/gpio/Watchdog\ Disable/value
mount -o rw,remount /
MYDIR=$(dirname "$(readlink -f $0)")

cp -a /usr/lib/gstreamer-0.10/libgstflac.so /usr/lib/gstreamer-0.10/libgstflac.so.orig 
cp -a /usr/lib/libFLAC.so.8.3.0 /usr/lib/libFLAC.so.8.3.0.orig 
cp -a /etc/race_mp_config.xml /etc/race_mp_config.xml.orig 
cp -a /etc/race_config_params_CMU.xml /etc/race_config_params_CMU.xml.orig
sleep 3
cp -a ${MYDIR}/libgstflac.so /usr/lib/gstreamer-0.10/libgstflac.so
cp -a ${MYDIR}/libFLAC.so.8.3.0 /usr/lib/libFLAC.so.8.3.0
cp -a ${MYDIR}/race_mp_config.xml /etc/race_mp_config.xml 
cp -a ${MYDIR}/race_config_params_CMU.xml /etc/race_config_params_CMU.xml
sleep 3
chmod 755 /usr/lib/gstreamer-0.10/libgstflac.so
chmod 755 /usr/lib/libFLAC.so.8.3.0
sleep 3
cd /usr/lib/
ln -s libFLAC.so.8.3.0 libFLAC.so.8

killall jci-dialog
sleep 3
/jci/tools/jci-dialog --confirm --title="Installation" --text="Complete! Click OK to reboot the system."
		if [ $? != 1 ]
		then
			reboot
			exit
		fi
sleep 10
killall jci-dialog
