#!/bin/sh
USB=$*
if [ "${USB}" = '' ]; then
  USB="TREXAR"
fi
cd ~/headunit/mazda && make clean && make
chmod -v 755 ~/headunit/mazda/headunit
ls -l
mv ~/headunit/mazda/headunit /media/mazda/${USB}/config/androidauto/data_persist/dev/bin/ && echo SUCCESS
sleep 5
exit 0
