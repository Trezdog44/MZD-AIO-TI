#!/bin/sh
echo '<!DOCTYPE html>
<style>.bootbox.update-info{margin-top: 0px;}</style>
<p><a href="changelog.htm" data-featherlight="ajax">Full Changelog</a></p>' > ./app/views/update.htm
md2html ./release-notes.md >> ./app/views/update.htm && echo SUCCESS
sleep 5
exit 0
# This is for something else
echo 1 > /sys/class/gpio/DO_HOST1_PWR/value &
echo 1 > /sys/class/gpio/DO_OTG_PWR/value &
killall splashplayer
killall svcjcinativeguictrl
killall wayland-viv-compositor
sleep 1
export WL_COMPOSITOR_CONFIG=/etc/wayland/compositor_testmode.conf
/usr/bin/wayland-viv-compositor &
waitfor /tmp/wayland-0
