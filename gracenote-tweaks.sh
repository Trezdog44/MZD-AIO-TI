#!/bin/sh

# disable watchdog and allow write access
echo 1 > /sys/class/gpio/Watchdog\ Disable/value
mount -o rw,remount /

show_message()
{
	killall jci-dialog
	#	log_message "= POPUP: $* "
	/jci/tools/jci-dialog --info --title="MESSAGE" --text="$*" --no-cancel &
	sleep 4
}

MYDIR=$(dirname "$(readlink -f "$0")")


/jci/tools/jci-dialog --3-button-dialog --title="Disable Gracenote!" --text="Install Disable Gracenote\n\nChoose Installation Method" --ok-label="Install" --cancel-label="Uninstall" --button3-label="Skip"
choice=$?

if [ $choice -eq 0 ]; then
	/jci/tools/jci-dialog --3-button-dialog --title="WARNING: THESE ARE EXPERIMENTAL!" --text="Choose Method\n1. Kill Gracenote Process (CAN BE UNINSTALLED)\2.DELETE GRACENOTE DATABASE (CAN NOT BE REVERSED)" --ok-label="Install" --cancel-label="Uninstall" --button3-label="Skip"
	choice2=$?
	if [ $choice2 -eq 0 ]; then
		if [ ! -e /jci/scripts/stage_usb.sh.org ]; then
			cp -a /jci/scripts/stage_usb.sh /jci/scripts/stage_usb.sh.org
		fi
		cp -a ${MYDIR}/config/disable_gracenote/* /jci/scripts/
		chmod 777 /jci/scripts/stage_usb.sh
		chmod 777 /jci/scripts/kill_gn.sh
		show_message "=====***** DISABLED GRACENOTE *****====="
	elif [ $choice2 -eq 1]; then
		rm -rf /tmp/media.db
		rm -rf /data/media.db
		rm -rf /data/natp/persistence/usbaudio_*
	else
		show_message "=======****** SKIPPED ******======="
	fi
	sleep 5
elif [ $choice -eq 1 ]; then
	if [ -e /jci/scripts/stage_usb.sh.org ]; then
		cp -a /jci/scripts/stage_usb.sh.org /jci/scripts/stage_usb.sh
	fi
	rm -f /jci/scripts/kill_gn.sh
	show_message "=====**** ENABLED GRACENOTE *****===="
else
	show_message "=======****** SKIPPED ******======="
fi
if [ $choice -ne 2 ] || [ $choice2 -ne 2 ]; then
	show_message "===**** REBOOTING ****==="
	sleep 2
	reboot
fi

exit 0
