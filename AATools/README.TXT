Patch 3rd Party Apps For Use With Android Auto v3:
Pre: Uninstall the app if it is already installed on your phone
1. Run AA+Patcher
2. Prompt: Connect Phone To PC Via USB Cable
3. Prompt: Enable USB Debugging In Developer Options (if it is not already enabled)
4. Copy apk File To Desktop
5. Rename apk File my.apk
6. Continue In AA+Patcher
7. The App Is Installed And Will Work With AA v3