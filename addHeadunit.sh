#!/bin/sh
BETA=$*
if [ "$BETA" != '' ]; then
  BETA=b
fi
cd ~/headunit/mazda && make clean && make
chmod -v 755 ~headunit/mazda/headunit
ls -l
mv -v ~/headunit/mazda/headunit ~/MZD-AIO-TI/app/files/tweaks/config/androidauto${BETA}/data_persist/dev/bin/ && echo SUCCESS
sleep 5
exit 0
