# AIO + CASDK

### v2.8.6

#### New Code Signing Certificate
- Cost me $70 so Donations are very appreciated!

#### Android Auto Headunit App v1.13
- Support for interleaved channel data
- Exit icon is now the Mazda Logo

#### Speedometer v6.1
- Minor positioning/sizing fixes

#### Remove Boot Animation
- Fixed for FW with "newLoopLogo"
